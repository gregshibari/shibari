# Dragonfly sleeve

## Description

This is a decorative tie of the arms to the back.

## Instructions

#. In the center of a rope, create two loops laid on top of each other as in the picture, such that the free ends have equal length.

	![Dragonfly sleeve - initial loops.](loops.png)

#. Pull the upper loop down through the lower loop, and the lower loop up through the upper loop, thus forming a handcuff knot (ABOK #1140).
#. Bring the arms to the back and lay the cuffs on the arms, pushed up to the shoulders, such that you obtain the following figure:

	![Dragonfly sleeve - shoulder cuffs.](shoulder.png)

#. Repeat the following pattern every 10-15cm along the arms:

	#. Make a slipped overhand knot (ABOK #529), such that the adjustable loop is formed by the free ends Y.
	#. Separate the two ropes forming the adjustable loop, and lay one of them around each arm.
	#. Pull the loops tight, bringing the arms possibly close together and create tension. The result of each step should be as follows:

	![Dragonfly sleeve - single step down the arms.](step.png)

#. When you have reached the wrists, wrap each of the free ends a few times around the cuffs on either side, to create "bars".
#. Fix the wraps with a half hitch (ABOK #48) on each side, so that you obtain the following tie:

	![Dragonfly sleeve - wrist cuffs.](wrists.png)

#. Bring the free ends A and B towards the front between the legs.
#. Pull each end through the shoulder cuff on the corresponding side, upwards parallel to the arm.
#. Make an overhand knot (ABOK #47) around the strands coming from A and B, respectively.
#. Adjust the position of the knots to set the tension. This should create the following figure:

	![Dragonfly sleeve - final knots.](finish.png)

## References

* <http://www.knottyboys.com/videos/Harnesses/Dragonfly_Sleeve.wmv>
