# Corselet harness

## Description

This harness combines several decorative elements: a corset, a double coin knot and a diamond shape as it appears in the hishi karada. Also it is rather simple to tie.

## Instructions

#. Begin at the front with the lower end of the corset. Start with the center bight and lay the ropes once around the waist from W to W.
#. Pull the ropes through the center bight, forming a lark's head (ABOK #5), and continue upwards to V to obtain this figure:

	![Corselet harness - lowest loop.](bottom.png)

#. To weave the corset, repeat the following steps:

	#. Coming from the previous loop V, reverse direction and make another loop from Q to Q, closely above the previous loop.
	#. Pull the rope ends first under the strand coming from V, then over the strand going to Q, reverse direction and make another loop from P to P, closely above the previous loop.
	#. Go first under, then over the newly created bight and then upwards to the next loop U. This should create such kind of pattern:

	![Corselet harness - single step for the corset part.](middle.png)

#. Once you have reached the lower part of the chest, go straight up to the upper part of the chest and make a double coin knot (ABOK #1428):

	![Corselet harness - upper front part.](top.png)

#. Split the rope ends and go over the shoulders A and B to the back.
#. At the back, tie another double coin knot (ABOK #1428):

	![Corselet harness - upper back part.](coin.png)

#. Bring the rope ends to the front under the arms towards C and D.
#. Pull the ends through the central loop formed under the double coin knot and pull it such that it takes a diamond form.
#. Bring the free ends back under the arms towards E and F.
#. Lock the tie off with a square knot (ABOK #1204):

	![Corselet harness - lower back part.](square.png)

## References

* <http://www.knottyboys.com/videos/Harnesses/Corselet_Harness.wmv>
