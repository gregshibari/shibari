# Lark's head single column tie

## Description

The [lark's head single column tie](../lark/) may be supported with extra wraps. These distribute the force over a larger area.

## Instructions

1. Start with the center bight and lay the rope once around the column from A to A.
2. Pull the ends through the center bight to form a lark's head (ABOK #5).
3. Reverse direction and lay the ropes once again around the column from B to B.
4. Add a few extra wraps B-C-D&hellip;
5. After the last wrap, go over all previous wraps and pull the ends through the double bight A-B.
6. Go back over all wraps A-B-C-D-&hellip;, then under all wraps &hellip;-D-C-B-A in the opposite direction.
7. Pull the ends up through the bight towards X and pull tight. The result should look like this:

	![Finished lark's head single column tie with extra wraps.](final.png)

Note that pull should be applied *to the left* when the tie is finished this way, since otherwise it is likely to roll up. If pull is supposed to be applied *to the right*, then the last two steps should be replaced like this:

6. Form a bight by going first over all wraps A-B-C-D-&hellip;, then back oven all wraps &hellip;-D-C-B-A in the opposite direction.
7. Pull the ends under all wraps A-B-C-D-&hellip;, then up through the bight from the previous step towards X and pull tight. Now the result should look like this:

	![Finished lark's head single column tie with extra wraps and opposite pull direction.](final2.png)

## References

* <http://crash-restraint.com/blog/2010/05/02/larks-head-based-single-column>
