# Somerville bowline

## Description

The Somerville bowline is a simple, yet safe and practical single column tie, and hence very suitable for beginners.

## Instructions

#. Start with (at least) two wraps of rope around the column, leaving about 15cm at the short end Y.
#. Create a loop in the long end X and lay it over the wraps A-A and B-B.
#. Pull the short end (coming from A at the bottom of the diagram) through the loop from below.
#. Wrap the short end once around the wraps - first over the long ends, then pull trough under the wraps, finally once more through the loop.
#. Pull both ends X and Y tight. You should have this result now:

	![Somerville bowline](final.png)

## References

* <http://crash-restraint.com/ties/66>
