# 2-rope Gote

## Description

This is one of many different ways how to tie a Takate Kote, and is rather basic. It combines different ways how to cross on the back and should be considered more educational than practical.

## Instructions

#. Place your partner's arms in the box tie position.
#. Tie a single column tie (SCT) around your partner's arms. Make sure that it is comfortable and not too tight.
#. Go to the left arm A, just underneath the shoulder, and lay the ropes around the upper part of the chest to the right arm B.
#. On the back, go first under the strand to A, then reverse direction and go over the same strand towards C. Make sure the strand going to the SCT is vertical and centered.
#. Go once more around the upper part of the chest from C to D, closely over the strand A-B.
#. On the back, go over the ropes to the lower right corner, then upwards under the bight B-C.
#. Cross over the ropes to the lower left corner, then upwards under the strands to A and D. The result should look like this:

	![2-rope Gote TK - after upper chest wraps.](back1.png)

#. Stay under all ropes so far on the back and go forward under the right armpit towards E.
#. Pull the rope ends to the front, keeping them over the strands A-B and C-D.
#. Cross downwards over the strands A-B and C-D towards F, and to the back under the armpits, hence creating a wrap around A-B and C-D. Make sure not to pull too tight.
#. On the back, make an upwards spiraling loop around the strand to the SCT, first crossing over it to the left, then under to the right and over to the left again.
#. Cross over the remaining ropes on the back forward under the left armpit G.
#. Pull the rope ends to the front, keeping them over the strands A-B and C-D.
#. Cross downwards over the strands A-B and C-D towards H, and to the back under the armpits, hence creating another wrap around A-B and C-D. Make sure not to pull too tight.
#. On the back, cross over the ropes to the upper right corner, then downwards under the bight B-C, left over the strand to the SCT and upwards under the strands going to A, D and H towards I. Now you should get this shape:

	![2-rope Gote TK - after upper chest kannukis.](back2.png)

#. Cross downwards over all ropes on the back towards the right arm I, closely over the elbows.
#. Make a wrap around both arms and the lower part of the chest towards J.
#. On the back, cross straight over the strand connecting the SCT and the upper part of the tie towards K.
#. Make another wrap around both arms and the lower part of the chest towards L, closely under the strand I-J.
#. Create a clockwise double loop on the back: first cross up right over the strands J-K and the strands to I and the SCT, then downwards under the strands to I and K, up left over the strands to J, L and the SCT, right under the strands to the SCT and I and finally down left crossing over all other ropes. You should obtain this pattern:

	![2-rope Gote TK - after lower chest wraps.](back3.png)

#. Bring the ropes to the front, passing between the left arm and the chest M, keeping the working ends under the strands I-J and K-L.
#. Cross upwards over the strands I-J and K-L towards N, and to the back between the left arm and chest, hence creating a wrap around I-J and K-L. Make sure not to pull too tight.
#. On the back, cross down right over the crossing formed by the vertical strand and the strands towards I, J, K and L.
#. Bring the ropes to the front, passing between the right arm and the chest O, keeping the working ends under the strands I-J and K-L.
#. Cross upwards over the strands I-J and K-L towards P, and to the back between the right arm and chest, hence creating another wrap around I-J and K-L. Make sure not to pull too tight.
#. On the back, cross to the left over the vertical strand connecting the upper and lower rope crossings.
#. Go down underneath the strands going towards J, L, M and N on the left.
#. Cross to the right again, over the vertical strand going down to the SCT.
#. Go upward, crossing over the strand to O, under the strands to I and L and over the strand to P.
#. Cross to the left over the vertical strand connecting upper and lower rope crossings.
#. Go further upwards, crossing under the strands going left to A, D, G and H.
#. Cross downwards over the strands going right towards B. C, E and F.
#. Tuck the rope ends Q under the last created crossing over the vertical strand and pull tight to lock the tie off. The final tie should look like this:

	![2-rope Gote TK - after lower chest kannukis.](back4.png)

	![2-rope Gote TK - final front view.](front.png)

## References

* <http://www.theduchy.com/theduchy-com-all-rights-reserved/>
