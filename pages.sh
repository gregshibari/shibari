#!/bin/bash

cwd=`pwd`

for folder in `find figures -type d`
do
	cd $folder

	shopt -s nullglob

	for file in *.tex
	do
		pdf=`echo $file | sed -e 's/tex$/pdf/'`
		png=`echo $file | sed -e 's/tex$/png/'`
		pdflatex $file
		convert -flatten -density 120 $pdf $png
	done

	for file in *.md
	do
		html=`echo $file | sed -e 's/md$/html/'`
		pandoc -c "/shibari/shibari.css" -T "`grep -Em1 '^#\s+.*$' $file | sed -Ee 's/^#\s+//'`" -s -f markdown -t html5 -o $html $file
	done

	shopt -u nullglob

	cd $cwd

	mkdir -p .public/$folder
	cp $folder/*.png .public/$folder/ 2>/dev/null || :
	cp $folder/*.html .public/$folder/ 2>/dev/null || :
done

cp shibari.css .public/
pandoc -c "/shibari/shibari.css" -T "Shibari" -s -f markdown -t html5 -o .public/index.html index.md
